import numpy as np
import cv2 as cv
import os
from PIL import Image, ImageDraw

global n
n = 1
i = 22
hsv_min = np.array((0, 0, 0), np.uint8)
hsv_max = np.array((0, 0, 100), np.uint8)


def bukvi(img, contours, k):
    x, y, w, h = cv.boundingRect(contours[k])
    cv.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
    cv.imwrite('scale.jpg', img)
    for i in range(h):
        for j in range(w):
            img[y + i, x + j] = (255, 255, 255)
    cv.imwrite('scale.jpg', img)
    return x, y, w, h


def cont(fn):
    img = cv.imread(fn)
    hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
    thresh = cv.inRange(hsv, hsv_min, hsv_max)
    _, contours, hierarchy = cv.findContours(thresh.copy(), cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    cv.drawContours(img, contours, -1, (255, 0, 0), 3, cv.LINE_AA, hierarchy, 1)
    return img, contours


def redraw(x, y, w, h, g):
    global n
    img = 'non.jpg'
    name_b = "A"
    img = cv.imread(img)
    image_cut = img[y:y + h, x:(x + w)]
    cv.imwrite("donut.jpg", image_cut)
    for i in range(h):
        for j in range(w):
            img[y + i, x + j] = (255, 255, 255)
    path = 'C:/Bukvis'
    img2 = cv.imread("donut.jpg")
    cv.imwrite("non.jpg", img)
    if g is True:
        n = n + 1
        naz = str(n) + ".jpg"
        cv.imwrite(os.path.join(path, naz), img2)


while i > -2:
    i = i - 1
    img, contours = cont("non.jpg")
    x, y, w, h = bukvi(img, contours, 0)
    if w < 20 or h < 20:
        i = i + 1
        redraw(x, y, w, h, False)
    else:
        redraw(x, y, w, h, True)
